/* Wriditng comment in javascript:*/
// one line comments - ctrl + /
/*multi-line comment - ctrl + shift + / */

console.log("Hello Batch 144!");

/*
Javascript - we can see or log messages in our console.

Browser Consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language - Javascript

For most browsers, consoles are easily accessed through its developer tools in the console tab.

In fact, consoles is browsers will allow us to add some Javascript expressions

Statement

Statements are instructions, expressions that we add to our programming language to communicate with our computers.

JS Statements usually ends in a semi colon (;), However, JS has implemented a way to automatically add semicolong at the end of a line/statement. 
Semi Colons can actually be ommited in creating JS statements but Semicolons in JS are added to mark the end of the statement.

Syntax

Syntax in programming is a set of rules that describes how statements are properly made/constructed

Lines/blocks of code must follow a certain of rules for it to work properly.


*/

console.log("Rae Dan");

// Mini activity
/*
console.log("Curry")
console.log("Curry")
console.log("Curry")
*/
let food1= "Curry";

console.log(food1);

/*
	Variables are a way to store information or data within our JS.

	To create a variable we first declare the name of the variable with either the let/const keyword:

	let nameOfVariable

	Then, we can initialize the variable with a value or data


	let nameOfVariable = <data>;
*/

console.log("My favorite food is: " + food1);


let food2 = "Karaage"


console.log("My favorite food are: " + food1 + " and " + food2);

let food3;

/*
	We can create variables without add an initial value, however, the variable's content is undefined.
*/

console.log(food3);

food3 = "Chickenjoy";

/*
	We can update the content of a let variable by reassigning the content using an assignment operator (=);

	assignment operator (=) = lets us assign data to a variable.
*/

console.log(food3);


food1 = "miso soup";

food2 = "Curry karaage";

console.log(food1);
console.log(food2);

/*
	we can update our variables with an assignment operator without needing to use the let keyword again.

	we cannot create another variable with the same name. It will result in an error
*/


// const keyword

/*
	const keyword will also allows us to create variables.
	However, with a const keyword we create constant variables, which means these data saved in a constant will not change, cannot be changed and should not be changed.
	
*/


const pi = 3.1416;

console.log(pi);


// trying to re-assign a const variable will result into an error;

// pi = "pizza";

// console.log(pi);


// const gravity;
// console.log(gravity);

/*
	We also cannot declare/create a const variable without initialization or an initial value.
*/


let myName;

const sunriseDirection = "East";

const sunsetDirection = "West";

/*
	You can actually log multiple items, variables, data in console.log seperated by ,
*/
console.log(myName, sunriseDirection, sunsetDirection);

/*
	Guides in creating a JS variable:

	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared.

	2. Creating a variable has two parts: Declaration of the variable name and Initialization of the initial value of the variable using an assignment operator (=)

	3. A let variable can be decalred without initialization. However the value of the variable will be undefined until it is re-assigned with a value.

	4. Not Defined vs Undefined. Not Defined error means the variable is used but NOT declared. Undefiend results from a variable that is used but is not initialized.

	5. We can use const keyword to create constant variables. Contant variables cannot be declared without initialization. Constant variables be re-assigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflitct with syntax in JS that is named with capital letters.

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add a space for variables with words as names.

*/
//Data Types

/*
	strings are data wich are alphanumerical text. It could be a name, a phrase or even a sentence. We can create string with a single quote('') or with double quote ("").

	variable = "string data"
*/

console.log("Sample String Data")

let country = "philippines";
let province = "rizal";

console.log(province,country);

/*
	Concatentation - is a way for us to add strings to together and have a single string. We can use our "+" symbol for this.
*/

let fullAddress = province + ',' + country;

console.log(fullAddress);

let greeting = "i live in " + country;

console.log(greeting);

/*
	In JS, when you use the + sign with strings we have concatenation
*/

let numString = "50";
let numString2 = "25";

console.log(numString + numString2);

/*
	Strings have property called .length and it tells us the number of characters in a string.

	Spaces, commas, periods can also be characters when included in a string.

	It will return a number type data.
*/

let hero = "Captain America";

console.log(hero.length);

//number type
/*
	Number type data can actually be used in proper mathematical equations and operations.
*/

let students = 16;
console.log(students)

let num1 = 50;
let num2 = 25;

/*
	Addition Operator will allow us to add two number types and return the sum as a number data type.

	Operations return a value and that value can be saved in a variable.

	What if we use the addition operator on a number type and a numerical string?
	It results to concatenation

	parseInt() will allow us turn a numeric string into a proper number type.
*/


let sum1 = num1 + num2;

console.log(sum1);

let numString3 = "100";

let sum2 = parseInt(numString3) + num1

console.log(sum2);

// mini activity

let sum3 = sum1 + sum2;

let sum4 = num2 + parseInt(numString2);

console.log(sum3);
console.log(sum4);

/*
	Subtraction operator -

	Will let us get the difference between two number type. It will also result to a proper number type data.

	When subtration operator us used on a string and a number, the string will be converted into a number automatically and then JS will perform the operation.

	This automatic coversion from one type to another is called Type Conversion or Forced Coercion.

	When a text string is subtracted with a number, it will result in NaN or Not a number beacause when JS coverted the text string it results to NaN and NaN-number = NaN;
*/
let difference = num1 - num2;

console.log(difference);

let difference2 = numString3 - num2;

console.log(difference2);

let difference3 = hero - num2;
console.log(difference3);

let string1 = "fifteen";

console.log(num2 - string1);


// multiplacation operator (*)

let num3 = 10;
let num4 = 5;


let product = num3*num4

console.log(product)

let product2 = numString3 * num3;

console.log(product2);

let product3 = numString3 * numString;

console.log(product3);

// Division Operator (/)

let num5 = 30;
let num6 = 3;
let quotient = num5/num6;
console.log(num5/num6);

console.log(quotient);

let quotient2 = numString3/5;
console.log(quotient2);
let quotient3 = 450/num4;
console.log(quotient3);

// boolean (true or false)

// Boolean is usually used for logical operations or for if-else conditions
// Name convention for a variable boolean is a yes or no question

let isAdming = true;
let isMarried = false;

/*

	You can actually name your variable any way you want however as the best practice it should be:

		1. appropriate.
		2. definitive of the value it contains.
		3. semantically correct.

		let pikachu = "Tee Jae Bengan Calinao"
		console.log(pikachu)
*/

// arrays

// arrays are a special kind of data type wherein we can store multiple values.
// an array can store multiple values and even of different types. For best practice, keep the data type of items in an array uniform.
// values in array are seperated by comma. Failing to do so, will result in an error
// an array is created with an array literal ([])

let koponanNiEugene = ["Eugene","Alfred","Dennis","Vincent"];

console.log(koponanNiEugene)

// array indices are markers of the order of the items in the array. array index starts at 0

// To access an array item: arrayName[index]
console.log(koponanNiEugene[0]);


// bad practice for an array:
let array2 = ["One Punch Man", true, 500, "Saitama"];

console.log(array2);


// Objects
/*
	Objects are another kind of special data type used to mimic real world objects.
	With this we can add information that makes sense, thematically relevant and of different data types.

	Objects can be created with Object Literals ({})

	Each value is given a label which makes the data significant and meaningful this paring of data and "label" is what we call a Key-Value pair.

	A key-value pair together is called a property.
*/

let person1 = {
	heroName: "One Punch Man",
	isRegistered: true,
	balance: 500,
	realName: "Saitama"
}

// to get the value of an object's property, we can access it using dot notation.
//objectName.propertyName

console.log(person1.realName)

// mini activity

let myFavoriteBands = ["Haruno","The Winking Owl","Nowisee","PsyQui","Aimer"];

let me = {
	firstName: "Rae Dan",
	lastName: "Buccat",
	isWebDeveloper: true,
	hasPortfolio: true,
	age: 24,
	// Properties of an object should relate to each other and thematically relevant to describe a single item/object
	brand: "Toyota"
}

// Undefined vs Null

// Null

// Is the explicit decalration that there is no value

let sampleNull = null;

// undefined

// means that the variable exists however was not initialized with the variable.

let undefinedSample;

console.log(undefinedSample);

// Certain processes in programming explicity return null to indicate that the task resulted to nothing

let foundResult = null;

// For undefined, this is normally caused by developers creating variables that have no value or data associated with them.
// the variable does exist but its value is still unknown.

let person2 = {
	name: "patricia",
	age: 28
}


console.log(person2.isAdmin)