//Function

function printStar() {
	console.log("*");
	console.log("**");
	console.log("***");
}

function sayHello(name) {
	console.log("Hello" + name);
}

// Function that accepts two numbers
// and prints the sum


//parameters
/*
function addSum(num1, num2) {
	let sum = num1 + num2;
	console.log(sum);
}
*/

// addSum(13,2); //arguments in a function
// addSum(23,56);


//function with 3 parameters
// String Template Literals
// Interpolation
/*
function printBio(lname,fname,age) {
	console.log("Hello " + lname, fname, age);
	console.log(`Hello ${lname} ${fname} ${age}`)
}
*/
//return keyword
function addSum(x, y) {
	return y-x
	console.log(x+y);
}

let sumNum = addSum(3,4);

/*
function getBioTest(lname, fname, email, mobileNum) {
	console.log(`Hello I am ${fname} ${lname}`);
	console.log(`You can contact me via ${email}`);
	console.log(`Mobile Number: ${mobileNum}`)

};

getBioTest("Rogers","Steve","capt_am@gmail.com","09171234567");
*/